<?php
/**
 * @file
 * Containg several form alters and other functions for expanding 
 * the campaign creation functionality of the Mailchimp module. 
 *
 */

/**
* Implements hook_entity_info_alter().
*/
function mailchimp_easy_campaign_entity_info_alter(&$entity_info) {
  $entity_info['node']['view modes']['mailchimp'] = array(
    'label' => t('Mailchimp'),
    'custom settings' => TRUE,
  );
}