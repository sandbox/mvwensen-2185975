<?php
/**
 * @file
 * The ajax functions for the mailchimp easy campaign module
 */

/**
 * function mailchimp_easy_campaign_html_by_nid
 * @description Function gets the html render in node_view mode by the given nids
 * @param none
 * @return $content JSON Object, containing the renderd
 */
function mailchimp_easy_campaign_html_by_nid() {

  // Get the argument with the nids to 
  $nids = check_plain(arg(2));
  $nids = explode("&amp;", $nids);
  $content = "";

  // Loop through nids, get the content and glue it together
  foreach ($nids as $key => $nid) {
    
    // Check if it's an integer
    if (intval($nid)) {
      // Load the noad by the nid
      $node = node_load($nid);

      // Load the noad_view with the 'mailchimp' view mode
      $view = node_view($node, 'mailchimp');

      // Glue the results together
      $content .= render($view);
    }

  }
  
  // Return the $content as json output
  drupal_json_output($content); 
}



/**
 * Autocomplete helper
 * @param String $string, string for search
 * @return $matches JSON object
 */
function mailchimp_easy_campaign_autocomplete($string) {

  // Get the node-types available
  $node_types = node_type_get_types();

  // Check the ones that are enabled in the admin  
  $node_types_check = array();

  foreach ($node_types as $key => $value) {

    $check = variable_get('mailchimp_easy_campaign_node_type_' . $key, FALSE);

    // If enabled add to array
    if ($check) {
      $node_types_check[$key] = $key;
    }

  }

  // Get the matching items from the database
  $matches = array();
  $result = db_select('node', 'n')
    ->fields('n', array('nid', 'title', 'created'))
    ->condition('title', '%' . db_like($string) . '%', 'LIKE')
    ->condition('type', $node_types_check, 'IN')
    ->orderBy('nid', 'DESC')
    ->execute();
  
  // Save the query to matches
  foreach ($result as $row) {

    // if changed, js has to be changed too!
    $matches['nid:' . $row->nid . '|' . check_plain($row->title)] = '[' . $row->nid . '] ' . check_plain($row->title . ' (' . format_date($row->created, 'small') . ')');

  }
  
  // Return the result to the form in json
  drupal_json_output($matches);

}