<?php

/**
 * @file
 * Mailchimp module admin settings.
 */

/**
 * function mailchimp_easy_campaign_admin_settings
 * @description Return the MailChimp global settings form for selecting the node types available on autocomplete
 */
function mailchimp_easy_campaign_admin_settings() {
  // Fetch all available node types
  $node_types = node_type_get_types();
  
  // Loop throught the node-types and create a checkbox for each
  foreach ($node_types as $key => $value) {

    // The form element for checking turning on the availability of the current node type
    $form['mailchimp_easy_campaign_node_type_' . $key] = array(
      '#type' => 'checkbox',
      '#title' => $value->name,
      '#default_value' => variable_get('mailchimp_easy_campaign_node_type_' . $key, TRUE),
      "#description" => t('Enable for Mailchimp campaigns.'),
    );

  }

  return system_settings_form($form);
}
