<?php
/**
 * @file
 * Adding the possibility to send testmails from Drupal 
 *
 */

/**
 * Implements hook_menu().
 */
function mailchimp_easy_campaign_test_menu_add() {
  $items = array();
  $items['admin/config/services/mailchimp/campaigns/%mailchimp_campaign/test'] = array(
    'title' => t('Send Test'),
    'description' => t('Test the current MailChimp campaign.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailchimp_easy_campaign_test_form', 5),
    'access callback' => 'mailchimp_campaign_access',
    'access arguments' => array(5, FALSE),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2
  );

  return $items;
}

/**
* Form for sending a testmail to a e-mailaddress 
*/
function mailchimp_easy_campaign_test_form($form, &$form_state, MailChimpCampaign $campaign = NULL) {
  $form_state['campaign'] = $campaign;
  
  // TODO: 
  // - Move inline stylig to css
  // - Somehow fetch the number of testmails left
  $form['email_address'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mailaddress'),
    '#description' => t('The E-mailaddress to send the to.') . ' <span class="warning" style="color:red;">' . t('Warning! A Maximum of 12 Test mails can be sended!') . '</span>',
    '#required' => TRUE,
    '#default_value' => '',
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send a Testmail!'),
    '#submit' => array('mailchimp_easy_campaign_test_form_submit'),
  );

  return $form;
}


function mailchimp_easy_campaign_test_form_submit($form, &$form_state) {

  // Get the Form variables
  $email_addresses = $form_state['values']['email_address'];
  $campaign = $form_state['campaign'];
  
  // Explode string with comma separated values into an array
  $email_addresses = array_map('trim', explode(',', $email_addresses));
 
  // Send test mail by MailChimp API, temporary only to the first mailaddress in the array. (Multiple not working correct in the API, it sends only to the last emailaddress)
  $sent = mailchimp_easy_campaign_test_send_campaign($campaign, array( $email_addresses[0]) );

    // Return with watchdog
  if ($sent) {
  // Log action, and notify the user.
    watchdog('mailchimp_campaign', 'MailChimp Testmail %name has been sent.',
      array('%name' => $campaign->label())
    );
  }
  
  //Redirect back to the edit page
  $form_state['redirect'] = '/';
}


/**
 * Send a test of this campaign to the provided email address (API function)
 * @param string $cid the id of the campaign to test
 * @param array $test_emails an array of email address to receive the test message
 * @param string $send_type optional by default (null) both formats are sent - "html" or "text" send 
 */
function mailchimp_easy_campaign_test_send_campaign(MailChimpCampaign $campaign, $email_addresses) {
  $mcapi = mailchimp_get_api_object();

  // Send the Test campaign using the API function.
  $sent = $mcapi->campaignSendTest($campaign->mc_campaign_id, $email_addresses);

  if ($mcapi->errorCode) {
    // Display and log error, if any.
    $message = 'Eep, Eep! MailChimp error. The Test has not been sent due to an error.';
    _mailchimp_campaign_mcapi_error_message($mcapi, $message);
  }

  if ($sent) {
    // Log action, and notify the user.
    drupal_set_message(t('Eep, Eep! MailChimp Testmail has been sended!'), 'status', FALSE);
    watchdog('mailchimp_campaign', 'Eep, Eep!, MailChimp Test for %name has been sended.',
      array('%name' => $campaign->label())
    );
  }

  return $sent;
}