<?php
/**
 * @file
 * Containg several form alters and other functions for expanding 
 * the campaign creation functionality of the Mailchimp module. 
 *
 */

// mailchimp_campaign_campaign_form
function mailchimp_easy_campaign_form_mailchimp_campaign_campaign_form_alter(&$form, &$form_state, $form_id) {

  // Put the action buttons in a var and replace them (temp)
  $actions = $form['actions'];
  unset($form['actions']);

  // If it has multiple content area's these ara
  $areas = array();
  foreach ($form['content'] as $key => $value) {
  
    // Check if it's a "html_" key
    if (strpos($key, 'html_') !== FALSE) {
      $areas[$key] = $value['#title'];
    }

  }

  // Add fieldset for the Easy content fields.
  $form['easy_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Easy Content'),
  );

  // If areas is empty, there's only one content area $form['content']['html']
  if (empty($areas)) {
    $areas['html'] = $form['content']['html']['#title'];
  }

  // Get the campaign id 
  $campaign_id = $form_state['campaign']->mc_campaign_id;

  // Loop through the contentarea's
  foreach ($areas as $key => $value) {
    
    // Add Markup for a cosmetic header
    $form['easy_content'][$key]['head'] = array('#markup' => '<br /><strong>' . $value . '</strong>');

    // Loop through $i to create some fields with if already saved, the content
    $i = 1;
    while ($i < 20) {

      // check if already some values are saved
      $var_value = variable_get('mailchimp_easy_campaign_' . $campaign_id . '_' . $key . '_' . $i, $form_state['input'][$key . '_' . $i], "");

      // Create fields for autocompleting the nids, attention: if changed, js has to be changed too!
      $form['easy_content'][$key][$key . "_" . $i] = array(
        '#id' => $key . '--' . $i,
        '#type' => 'textfield',
        '#title' => check_plain($value . ' item: ' . $i),
        '#maxlength' => 128,
        '#default_value' => $var_value,
        '#autocomplete_path' => 'mailchimp-easy-campaign/autocomplete',
        '#attributes' => array('class' => array('mailchimp-easy-input ' . $key . '_' . $i . ' ' . $key)),
      );
      
      // Next please
      $i++;
      
      // If empty string, break the while loop so there is one empty field created
      if ($var_value == "") {
        break;
      }

    }

    // Add markup + Add more link
    $form['easy_content'][$key]['break'] = array(
      '#markup' => '<a href="#" data-field-name="' . $key . '" data-number-of-fields="' . $i . '" class="add-more-easy-input-rows">+ ' . t('Add more') . '</a><br />');
  }

  // Put the action buttons at the end of the array
  $form['actions'] = $actions;

  // Override the default submit handling
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save as draft'),
    '#submit' => array('mailchimp_easy_campaign_campaign_form_validate'),
  );
}


/**
 * Validation callback for mailchimp_campaign_campaign_form().
 * @decription Also saves the autocomplete fields in vars  
 * @param $form
 * @param $form_state
 */
function mailchimp_easy_campaign_campaign_form_validate($form, &$form_state) {
  $campaign_id = $form_state['campaign']->mc_campaign_id;
  
  // Start saving the data added via the easy_content fields
  foreach ($form_state['values']['content'] as $field_name => $array) {

  // counters user for reordering the fields
  $i = 1;
  $new_key = 1;

  // Test if fields like "$field_name_$i" are present, if set save the var in $new_key order
    while ($i <= 20) {

      // Check if the array value isset so we can save it.
      if (isset($form_state['input'][$field_name . '_' . $i]) && "" != $form_state['input'][$field_name . '_' . $i]) {

        variable_del('mailchimp_easy_campaign_' . $campaign_id . '_' . $field_name . '_' . $i);
        variable_set('mailchimp_easy_campaign_' . $campaign_id . '_' . $field_name . '_' . $new_key, $form_state['input'][$field_name . '_' . $i]);

        // After saving, $new_key++ 
        $new_key++;
      }
      else {
        variable_del('mailchimp_easy_campaign_' . $campaign_id . '_' . $field_name . '_' . $i);
      }

      // Feeds the while loop
      $i++;

    }

  }
    
  // Back to the regular saving spree!
  mailchimp_campaign_campaign_form_submit($form, $form_state);
}

