/**
* @file
* 
* Written by Martijn van Wensen <martijn@merge.nl>
* http://www.merge.nl
*/

(function ($) {

  Drupal.mailchimp_easy_campaign = {}
  
  Drupal.behaviors.mailchimp_easy_campaign = {
    attach: function(context, settings) {

      // Triggers only on document load
      if (context.nodeName == '#document') {

        // Collapse Content areas's
        $('.collapsible').toggleClass('collapsed');

        // Shizzle happens when clicked on "+ Add more" button (cloning last field, replacing key and reactivate ajax/onchange)
        $('.add-more-easy-input-rows', context).click(function() { 
          // Get some vars!
          var orig_field_name = $(this).attr('data-field-name');
          console.log(orig_field_name);
          var field_name = orig_field_name.replace('_','-');
          console.log(field_name);
          var new_number = $(this).attr('data-number-of-fields');
          var number = new_number-1;
          
          // Wrapper name
          var classname = '.form-item-'+field_name+'-'+number;
          var newclassname = '.form-item-'+field_name+'-'+new_number;
          var new_name = '#'+orig_field_name+'--'+new_number;
          
          // Get the html from the latest form field element. and clone it! Replace the keys so it's a unique field
          var copy = $(classname)[0].outerHTML
          copy = copy.split(number).join(new_number);
          $(classname).after(copy);

          // Deletes flag autocomplete processed
          $(new_name+'-autocomplete').removeClass('autocomplete-processed');
                  
          // Add onchange handler & autocompletehandler activation
          Drupal.attachBehaviors($(''+newclassname+''));

          // Up, the counter
          new_number++;
          $(this).attr('data-number-of-fields', new_number);

          // Do nothing
          return false;
        });
      }

      $('#edit-template-id', context).change(function() { 
        // TODO: Remove inline styling
        $(this).after('<span style="color:red;" class="mailchimp_easy_campaign_warning"> Save the campaign to activate the changes and fetch the correct fields.</span>');
      });

      // Check if textfields are changed or onblurred
      $('.mailchimp-easy-input', context).blur(function() { 
        var field = $(this);
        onInputChange(field);
      });

      function onInputChange(field){

        // Get the first part of the source_id name for identifying the content field
        var source_id = $(field).attr('id');
        var source = source_id.split('--');
        var source = source[0];
          
        // The content that is going to be placed into the content field.
        var content = "";
        var nids = "";

        // Loop the other fields and gather the nids in one array!
        var i = 0;

        $("."+source).each(function(){
          // Get the value from this field
          var value = $(this).val();

          if(value !== undefined){
            if(value.length > 0){
              // @TODO betterder validate value format.
              var split = value.split('|');
              var nid = split[0].split(':');
              nids = nids+parseInt(nid[1])+'&';
              i++;
            }
          }
        });
        if(nids !== ""){
          $.ajax({
            url: '/mailchimp_easy_campaign/get-html-by-nid/'+nids,
            type: 'POST',
            success: function(data) {
              console.log($(".form-textarea[name*="+source+"]").html());
              $(".form-textarea[name*="+source+"]").html(data);
            }
          });
        }
      }
    }
  }
}(jQuery));
